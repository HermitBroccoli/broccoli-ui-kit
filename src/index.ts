import './style.css'
import button from './components/Button.vue'

export default {
    install: (app: any, options: any) => {
      app.component('ui-btn', button)
    }
}