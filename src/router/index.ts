import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'main',
        component: () => import('../Pages/MainPages.vue')
    },
    {
        path: '/button',
        name: 'button',
        component: () => import('../Pages/Button.vue')
    },
    {
        path: '/group-button',
        component: ()=> import('../Pages/GroupButton.vue')
    },
    {
        path: '/input',
        component: () => import('../Pages/Input.vue')
    }
]

const router = createRouter({
    routes,
    history: createWebHistory(),
})

export default router